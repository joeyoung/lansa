﻿ServerModule:
  Name:               DF_T68DSO
  Target Platform:    Windows/IBM i/Linux

  Framework:
    Id1:                10240
    Id2:                30

  Multilingual Details:
  - ENG:
      Description:        "\\OS=Server Module"

  Source: |
    <-
    
    Begin_Com Role(*EXTENDS #PRIM_SRVM) Sessionidentifier('VLFONE')
    Define Field(#returnCode) Reffld(#VF_ELRETC)
    Define Field(#NewFileName1) Reffld(#DF_ELFNAM)
    
    * ---------------------------------------------------------------------------------------
    Srvroutine Name(uSaveToDatabase) Desc('Save a Blob in a database file') Session(*REQUIRED)
    Field_Map For(*Input) Field(#DF_ELBLOB)
    Field_Map For(*output) Field(#returnCode)
    
    
    * at this point we can store the blob in a database file or anywhere on the IFS of the server
    
    #DF_ELURL := #DF_ELBLOB.FileName
    
    Check_For In_File(DFT68BLOB) With_Key(#DF_ELBLOB.FileName)
    If_Status Is(*EQUALKEY)
    Update Fields(#DF_ELURL #DF_ELBLOB) In_File(DFT68BLOB) With_Key(#DF_ELURL)
    Else
    Insert Fields(#DF_ELURL #DF_ELBLOB) To_File(DFT68BLOB)
    Endif
    
    If_Status Is(*okay)
    #returnCode := OK
    Else
    #returnCode := ER
    Endif
    
    Endroutine
    
    * ---------------------------------------------------------------------------------------
    Srvroutine Name(uSaveAsFile) Desc('Save a Blob in the IFS as a file') Session(*REQUIRED)
    Field_Map For(*Input) Field(#DF_ELBLOB)
    Field_Map For(*output) Field(#returnCode)
    Field_Map For(*output) Field(#NewFileName1)
    
    Define_Com Class(#prim_alph) Name(#NewFileName)
    Define_Com Class(#std_int) Name(#LastSlashPos)
    
    
    * at this point we can store the blob in a database file or anywhere on the IFS of the server
    
    * In this example we are saving the file in the partition execute directory on the IFS of the server.
    * But because we are using a server module we can put the file anywhere we are authorised to
    
    * Make a new name for the file based on its blob.Filename
    #DF_ELFNAM := #DF_ELBLOB.FileName
    
    * Remove everything up to the last \ (or / depending on the operating system)
    If (*OSAPI *EQ IBMI)
    #LastSlashPos := #DF_ELFNAM.LastPositionOf( '/' )
    Else
    #LastSlashPos := #DF_ELFNAM.LastPositionOf( '\' )
    Endif
    If (#LastSlashPos *NE 0)
    #DF_ELFNAM := #DF_ELFNAM.substring( (#LastSlashPos + 1) )
    Endif
    
    #NewFileName := *PART_DIR_EXECUTE + 'DF_T68_Temp_' + #DF_ELFNAM
    
    Use Builtin(OV_FILE_SERVICE) With_Args(COPY_FILE #DF_ELBLOB.FileName #NewFileName) To_Get(#returnCode)
    Use Builtin(OV_FILE_SERVICE) With_Args(SET_FILE #NewFileName NORMAL) To_Get(#returnCode)
    
    #NewFileName1 := #NewFileName
    
    Endroutine
    
    * ---------------------------------------------------------------------------------------
    * Check for the existence of a file on the server
    Srvroutine Name(uCheckFile) Desc('Check that a file exists on the server') Session(*REQUIRED)
    Field_Map For(*Input) Field(#DF_ELFNAM) Parameter_Name(FileName)
    Field_Map For(*OUTPUT) Field(#returnCode) Parameter_Name(ReturnCode)
    
    Use Builtin(OV_FILE_SERVICE) With_Args(CHECK_FILE (*PART_DIR_EXECUTE + #DF_ELFNAM)) To_Get(#returnCode)
    
    Endroutine
    
    * ---------------------------------------------------------------------------------------
    * Return a file to the client as a Response
    Srvroutine Name(uDownloadFileAsResp) Response(#Response) Desc('Download file from the server as response') Session(*REQUIRED)
    Field_Map For(*Input) Field(#DF_ELFNAM) Parameter_Name(FileName)
    
    #Response.ContentFile := *PART_DIR_EXECUTE + #DF_ELFNAM
    #Response.AttachmentFileName := #DF_ELFNAM
    Endroutine
    
    * ---------------------------------------------------------------------------------------
    * Return a file to the client as a blob field
    Srvroutine Name(uDownloadFileAsBlob) Desc('Download a file from the server as a blob field') Session(*REQUIRED)
    Field_Map For(*Input) Field(#DF_ELFNAM) Parameter_Name(FileName)
    Field_Map For(*OUTPUT) Field(#DF_ELBLOB) Parameter_Name(uBlob)
    
    #DF_ELBLOB := *PART_DIR_EXECUTE + #DF_ELFNAM
    
    Endroutine
    
    * ---------------------------------------------------------------------------------------
    * Build a text file on the server - return it as  a response
    Srvroutine Name(uMakeTextFileAsResp) Response(#Response) Desc('Make a text file and send it to the client as response') Session(*REQUIRED)
    
    Define_Com Class(#prim_alph) Name(#toFile)
    Define_Com Class(#prim_dc.UnicodeString) Name(#CRLF)
    #CRLF := (13).AsUnicodeString + (10).AsUnicodeString
    
    * Make a 3 line text file
    #Response.ContentString := ('Line 1 - This text was generated on the server' + #CRLF).AsNativeString
    #Response.ContentString += ('Line 2' + #CRLF).AsNativeString
    #Response.ContentString += ('Line 3' + #CRLF).AsNativeString
    
    #Response.AttachmentFileName := 'DF_T68_Server_Text_File.txt'
    #Response.ContentType := "text/plain"
    Endroutine
    
    * ---------------------------------------------------------------------------------------
    * Build a CSV file on the server - return it as  a response
    Srvroutine Name(uMakeCSVFileAsResp) Response(#Response) Desc('Make a CSV file and send as a response') Session(*REQUIRED)
    Field_Map For(*INPUT) Field(#UseCSVSeparator)
    
    Define Field(#UseCSVSeparator) Type(*NVARCHAR) Length(5)
    
    Define_Com Class(#prim_dc.UnicodeString) Name(#CRLF)
    
    #CRLF := (13).AsUnicodeString + (10).AsUnicodeString
    
    * This an acceptable way to tell MS-Excel what the separator is - staring with sep=x.
    * Typically it is a comma, the C is CSV, but in some systems in Southeast Asia this not always true.
    * So VLF-ONE client code can pass what they want to use.
    #Response.ContentString := ("sep=" + #UseCSVSeparator + #CRLF).AsNativeString
    
    * Now we just add the CSV values as "value","value","value" etc.
    * Line 1
    #Response.ContentString += ('"' + 'Line1_Cell1' + '"' + #UseCSVSeparator + '"' + 'Line1_Cell2' + '"' + #CRLF).AsNativeString
    * Line 2
    #Response.ContentString += ('"' + 'Line2_Cell1' + '"' + #UseCSVSeparator + '"' + 'Line2_Cell2' + '"' + #CRLF).AsNativeString
    * Line3
    #Response.ContentString += ('"' + 'Line3_Cell1' + '"' + #UseCSVSeparator + '"' + 'Line3_Cell2' + '"' + #CRLF).AsNativeString
    
    * Now finalize what the response is
    #Response.AttachmentFileName := 'DF_T68_Server_CSV_File.csv'
    #Response.ContentType := 'text/csv'
    
    Endroutine
    
    End_Com
