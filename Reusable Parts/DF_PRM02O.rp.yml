﻿Reusable Part:
  Name:               DF_PRM02O
  Enabled For RDMLX:  Yes
  Target Platform:    Browser

  Framework:
    Id1:                10240
    Id2:                30

  Multilingual Details:
  - ENG:
      Description:        "\\OC=5250 Prompter - 2 - Radio buttons"

  Source: |
    <-
    
    * ----------------------------------------------------------------------------------------------------------
    *
    *   ########  ##       ########    ###     ######  ########       ##    ##  #######  ######## ########
    *   ##     ## ##       ##         ## ##   ##    ## ##             ###   ## ##     ##    ##    ##
    *   ##     ## ##       ##        ##   ##  ##       ##             ####  ## ##     ##    ##    ##
    *   ########  ##       ######   ##     ##  ######  ######         ## ## ## ##     ##    ##    ######
    *   ##        ##       ##       #########       ## ##             ##  #### ##     ##    ##    ##
    *   ##        ##       ##       ##     ## ##    ## ##             ##   ### ##     ##    ##    ##
    *   ##        ######## ######## ##     ##  ######  ########       ##    ##  #######     ##    ########
    *
    *  This components DF_ or UF_ name prefix means it is a shipped demonstration or user example component.
    *  You must not use it in your production applications because it will change in future VLF versions.
    *  Instead, copy this code into your own VL object, which is not prefixed VF_, DF_ or UF_, and use that.
    *
    * ----------------------------------------------------------------------------------------------------------
    *
    *  Component    : DF_PRM02O
    *  Type         : Form
    *  Ancestor     : VF_AC017O
    *  Description  : This object is a sample "5250 prompter". It is supplied with
    *  source code as an example only. No warranty is expressed or
    *  implied. You should not use this component as is in production
    *  systems as it may change from VLF version to version. Copy this
    *  source into your own VL component and then modify as required.
    *
    *  This component is a pop up panel and it must extend the pop up panel class #VF_AC017O
    
    Begin_Com Role(*EXTENDS #VF_AC017O) Height(137) Width(258) Layoutmanager(#TableLayout1)
    
    * Layout management
    
    Define_Com Class(#PRIM_TBLO) Name(#TableLayout1)
    Define_Com Class(#PRIM_TBLO.Column) Name(#Column1) Displayposition(1) Parent(#TableLayout1)
    Define_Com Class(#PRIM_TBLO.Row) Name(#Row1) Displayposition(1) Parent(#TableLayout1)
    Define_Com Class(#PRIM_TBLO.Item) Name(#GroupBoxItem1) Manage(#GroupBox) Parent(#TableLayout1) Row(#Row1) Column(#Column1)
    
    *  Define the group box to contain the radio buttons
    
    Define_Com Class(#PRIM_GPBX) Name(#GroupBox) Displayposition(1) Height(135) Left(0) Parent(#COM_OWNER) Tabposition(1) Tabstop(False) Top(0) Width(256)
    
    *  Keep a collection of radio buttons
    
    Define_Com Class(#Prim_kCol<#PRIM_RDBN #Std_Code>) Name(#CollectRadioButtons) Style(Collection)
    
    *  ===========================================================================
    *  Handle an initialization request by filling the group box with example data
    *  ===========================================================================
    
    Mthroutine Name(uInitialize) Options(*Redefine)
    
    *  Initialize any empty group box height
    
    #GroupBox.Height := 12
    
    #GroupBox.Caption := 'Item Sizes'
    
    *  Put some example information into the group box as radio buttons
    
    #Com_Owner.CreateRadioButton Code(SML) Description('Small')
    #Com_Owner.CreateRadioButton Code(MED) Description('Medium')
    #Com_Owner.CreateRadioButton Code(LGE) Description('Large')
    #Com_Owner.CreateRadioButton Code(XL) Description('Extra Large')
    #Com_Owner.CreateRadioButton Code(XXL) Description('Extra Extra Large')
    
    *  Finalize the height of the group box
    
    #GroupBox.Height += 4
    
    Endroutine
    
    *  ===================================
    *  Method to create a new radio button
    *  ===================================
    
    Mthroutine Name(CreateRadioButton)
    Define_Map For(*input) Class(#Std_Code) Name(#Code)
    Define_Map For(*input) Class(#Std_Desc) Name(#Description)
    
    Define_Com Class(#PRIM_RDBN) Name(#NewRadioButton) Reference(*Dynamic)
    
    *  Manufacture a new radio button
    
    Set_Ref Com(#NewRadioButton) To(*Create_as #Prim_RdBn)
    
    *  Set it up for display
    
    Set Com(#NewRadioButton) Parent(#GroupBox) Height(20) Width(#GroupBox.Width - 16) Top(#GroupBox.Height) Left(8) Caption(#Description + " - " + #Code) Componenttag(#Code)
    
    *  Track it in the collection
    
    Set_Ref Com(#CollectRadioButtons<#Code.Value>) To(#NewRadioButton)
    
    *  Increment the height of the group box
    
    #GroupBox.Height += 20
    
    Endroutine
    
    *  ===========================================================================
    *  Handle a show request by setting the group box up and displaying it
    *  ===========================================================================
    
    Mthroutine Name(uShow) Options(*Redefine)
    
    Define_Com Class(#Std_Num) Name(#MatchCount)
    Define_Com Class(#Std_Num) Name(#BestMatchCount)
    Define_Com Class(#PRIM_RDBN) Name(#BestMatchRadioButton) Reference(*Dynamic)
    Define_Com Class(#Std_Code) Name(#PromptFieldValue)
    
    *  Get the value of the field being prompted.
    *  This is a "generic" prompter that can be used on any field,
    *  so the property uPrompt5250Field is used to find out the
    *  symbolic name of the field being prompted.
    
    Invoke Method(#Com_Owner.uGet5250Field) Name(#Com_Owner.uPrompt5250Field) Value(#PromptFieldValue) Index(#Com_Owner.uPrompt5250Index)
    
    *  Uncheck all the radio buttons
    
    Set Com(#CollectRadioButtons<>) Buttonchecked(False)
    
    *  Now locate and check the radio button that most closely matches
    *  what the user has already typed into the promptable field
    
    #BestMatchCount := 0
    #BestMatchRadioButton <= *Null
    
    For Each(#RadioButton) In(#CollectRadioButtons) Key(#Key_Code)
    
    *  Default to use the alphabetically first button if none match
    
    If (#BestMatchRadioButton *Is *null)
    #BestMatchRadioButton <= #RadioButton
    Endif
    
    *  See how this radio button matches
    
    #MatchCount := #Com_Owner.uMatch( #PromptFieldValue #Key_Code 3 )
    
    *  If this is a better match than anything so far make it the one to select
    
    If (#MatchCount > #BestMatchCount)
    #BestMatchCount := #MatchCount
    #BestMatchRadioButton <= #RadioButton
    Endif
    
    Endfor
    
    *  Check the best match found radio button
    
    #BestMatchRadioButton.ButtonChecked := True
    
    
    * Size and show the popup
    
    Set Com(#Com_Owner) Height(#GroupBox.Height + 4) Visible(True)
    #Com_Owner.ShowPopup Placement(Absolute) Top(#Top) Left(#Left)
    
    *  Finally cause the selected radio button to become the focus
    
    Invoke Method(#BestMatchRadioButton.SetFocus)
    
    Endroutine
    
    *  ================================================================
    *  Handle the selection of a radio button by Click
    *  ================================================================
    
    Evtroutine Handling(#CollectRadioButtons<>.Click) Com_Sender(#ClickedRadioButton)
    
    *  Pass the value of the clicked radio button back to the 5250 screeen
    
    #Com_Owner.uSet5250Field Name(#Com_Owner.uPrompt5250Field) Value(#ClickedRadioButton.ComponentTag) Index(#Com_Owner.uPrompt5250Index)
    
    #Com_Owner.uHide
    
    Endroutine
    
    End_Com
