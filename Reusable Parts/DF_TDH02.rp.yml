﻿Reusable Part:
  Name:               DF_TDH02
  Enabled For RDMLX:  Yes
  Target Platform:    Windows/IBM i/Linux

  Framework:
    Id1:                10240
    Id2:                110

  Multilingual Details:
  - ENG:
      Description:        "Country Code Table Data Handler"

  Source: |
    Function Options(*DIRECT)
    Begin_Com Role(*EXTENDS #VF_AC024)
    
    * This is an example Reusable Part Code Table Data Handler. The table data represents several country names in the native language.
    
    * The default behaviour is to read and write Code Table data to and from the shipped VLF Code Table Data files FPTAB and FPTABU. FPTABU is for Unicode table values and is linked to the main file, FPTAB, by the key fields. See the ancestor component, #VF_AC024, for a demonstration of how to read and write values to FPTABU.
    
    *  This example custom data handler was created by copying the component, UF_TDH01. Default behavour was changed by removing the calls to the ancestor methods and adding logic specific to the desired data source to the redefined methods.
    
    * =============================================================================
    * Field Definitions
    * =============================================================================
    Define Field(#uCountryDesc) Reffld(#VF_ELXVI2) Desc('Country Name in English.')
    Define Field(#uDiscard1) Reffld(#VF_ELXVI2) Desc('This value is not needed.')
    Define Field(#uCountryName) Reffld(#VF_ELUVI1) Desc('Country Name in native language.')
    Define Field(#uDiscard2) Reffld(#VF_ELUVI1) Desc('This value is not needed.')
    Define Field(#uKEYNo) Reffld(#STD_NUM)
    
    * =============================================================================
    * List definitions
    * =============================================================================
    Def_List Name(#CountryList) Fields(#uCountryDesc #uDiscard1 #uCountryName #uDiscard2) Type(*Working) Entrys(100)
    
    Mthroutine Name(uLoadTableData) Options(*REDEFINE)
    
    * Do the ancestor actions First. NOTE: This call should be included in custom data handlers.
    
    #COM_ANCESTOR.uLoadTableData Tablename(#TableName)
    
    * Check if entries exists for this table
    
    Check_For In_File(FPTABNMA) With_Key(#TableName)
    
    If_Status Is(*EQUALKEY)
    
    * If entries exis for this table in FPTAB use the default load method
    
    #COM_ANCESTOR.uDefaultLoad Tablename(#TableName)
    
    Else
    
    * Use a custom method that loads the table data from a flat file.
    
    #COM_OWNER.uLoadDataFromFile Tablename(#TableName)
    
    Endif
    
    Endroutine
    
    * The Code Table that this handler serves is read only so this method is not operational.
    
    Mthroutine Name(uSaveTable) Options(*REDEFINE)
    
    * Save the whole table back to the source
    
    * Perform default ancester actions. OPTIONAL.
    
    * #COM_ANCESTOR.uSaveTable
    
    * Set the I/O status - this must be done.
    #IO$STS := #VF_ELRETC
    
    Endroutine
    
    Mthroutine Name(uLoadDataFromFile) Help('Load Data from the shipped file, DF_UnicodeUTF8.dat')
    Define_Map For(*INPUT) Class(#FP_ETABL) Name(#TableName)
    
    Define Field(#uDesc_Prev) Reffld(#VF_ELXVI2) Desc('Previous Country Description.')
    Define Field(#uName_Prev) Reffld(#VF_ELUVI1) Desc('Previous Country Name.')
    
    Clr_List Named(#CountryList)
    
    Use Builtin(TRANSFORM_FILE) With_Args(#CountryList (*PART_DIR_EXECUTE + 'df_unicodeutf8.dat') OU) To_Get(#vf_elretc)
    
    * The first entry from this file is not wanted
    
    Dlt_Entry Number(1) From_List(#CountryList)
    
    * Now clean up any duplicate country names
    
    #uDesc_Prev := *BLANK
    #uName_Prev := *BLANK
    
    Selectlist Named(#CountryList)
    
    If Cond((#uCountryDesc *EQ #uDesc_Prev) *And (#uCountryName *EQ #uName_Prev))
    
    Dlt_Entry From_List(#CountryList)
    
    Else
    
    #uDesc_Prev := #uCountryDesc
    #uName_Prev := #uCountryName
    
    Endif
    
    Endselect
    
    #uKEYNo := 0
    
    Selectlist Named(#CountryList)
    
    #uKEYNo += 1
    
    * Signal the event uAddKey for each table row key. 5 Alpha and 5 numeric keys can be specified.
    
    Signal Event(uAddKey) Fp_Keytag(#uKEYNo) Tableakey1('') Tablenkey1(#uKEYNo)
    
    * Use the #COM_ANCESTOR.uAddEntry method to add a table column value for 'Descrition' to the table. NOTE: The appropriate keys and a value must always be passed to this method.
    
    #COM_ANCESTOR.uAddEntry Tablename(#TableName) Rowno(#uKEYNo) Tablenkey1(#uKEYNo) Valuename('DESCRPTN') Valuetype(A) Alphavalue(#uCountryDesc)
    
    * Use the #COM_ANCESTOR.uAddEntry method to add a table column value for 'Descrition' to the table. NOTE: The appropriate keys and a value must always be passed to this method.
    
    #COM_ANCESTOR.uAddEntry Tablename(#TableName) Rowno(#uKEYNo) Tablenkey1(#uKEYNo) Valuename('NAME') Valuetype(U) Unicodevalue(#uCountryName)
    
    Endselect
    
    * Use the ancestor save method to write the table data out to the shipped VLF Code Table database files FPTAB and FPTABU.
    
    #COM_ANCESTOR.uSaveTable
    
    * This must be set for any custom data loading method
    
    #IO$STS := OK
    
    Endroutine
    
    End_Com
