# LANSA

LANSA Partition to hold different apps and experiements.

Since a Partition contains multiple applications and all the LANSA stuff, the repo gets pretty crowded. 
Below are different applications in the Partition. 
Each application has a commit link to take you right to the squashed commits of the final app.

## Applications

### Tour Of Heroes
My version of Angular's great intro app Tour Of Heroes done in Visual LANSA.

[Commit](https://bitbucket.org/joeyoung/lansa/commits/11585fd05fcad487bb403b64e469a4031a280c3d)